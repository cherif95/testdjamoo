import 'package:flutter/material.dart';

class SearchBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: new BorderRadius.circular(16.0),
          border: Border.all(color: Colors.grey,width: 1.0)
      ),
      height: 50.0,
      child: Padding(
        padding: const EdgeInsets.only(left:8.0,right: 8.0),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Icon(Icons.search),
            ),
            Expanded(
              flex: 10,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: new BorderRadius.circular(8.0),
                    color: Colors.grey
                ),
                child: Text(""),
              ),
            )
          ],
        ),
      ),
    );
  }
}
