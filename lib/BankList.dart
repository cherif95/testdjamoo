import 'package:flutter/material.dart';
import 'package:testdjamo/widgets/GoogleMapWidget.dart';
import 'package:testdjamo/widgets/SearchBar.dart';

class BankList extends StatefulWidget {
  @override
  _BankListState createState() => _BankListState();
}

class _BankListState extends State<BankList> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: FlatButton.icon(
              onPressed: null,
              icon: Icon(Icons.arrow_back,size: 20.0,color: Colors.white),
              label: Text(
                "Withdraw money",
                style: TextStyle(fontWeight: FontWeight.w700,color: Colors.white,fontSize: 16.0),
              )
          ),
          actions: <Widget>[
            Padding(
              child: Text(
                "Show map",
                style: TextStyle(color: Colors.white),
              ),
              padding: EdgeInsets.fromLTRB(0, 20.0, 20.0, 0)
            )
          ],
        ),

        body: ListView(
          scrollDirection: Axis.vertical,
          padding: const EdgeInsets.all(15.0),

          children: <Widget>[

            SearchBar(),

            SizedBox(height: 20.0),

            Container(
              decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(8.0),
                  border: Border.all(color: Colors.grey,width: 1.0)
              ),
              height: 65.0,
              child: ListTile(
                leading: Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: new BorderRadius.circular(5.0),
                      border: Border.all(color: Colors.grey,width: 1.0)
                  ),
                ),

                title: RichText(
                  text: TextSpan(
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                            text: "Guichet Ecobank ",
                            style: TextStyle(fontWeight: FontWeight.w700),
                        ),
                        TextSpan(text: "0.5 km")
                      ],
                  ),
                ),
                subtitle: Text("Boulevard de marseille"),
                trailing: IconButton(
                    icon: Icon(Icons.arrow_forward_ios,size: 16.0,),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (content)=>GoogleMapWidget()));
                    }
                )
              ),
            ),

            SizedBox(height: 20.0),

            Container(
              decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(8.0),
                  border: Border.all(color: Colors.grey,width: 1.0)
              ),
              height: 65.0,
              child: ListTile(
                  leading: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: new BorderRadius.circular(5.0),
                        border: Border.all(color: Colors.grey,width: 1.0)
                    ),
                  ),

                  title: RichText(
                    text: TextSpan(
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Guichet Ecobank ",
                          style: TextStyle(fontWeight: FontWeight.w700),
                        ),
                        TextSpan(text: "0.5 km")
                      ],
                    ),
                  ),
                  subtitle: Text("Boulevard de marseille"),
                  trailing: IconButton(
                    icon: Icon(Icons.arrow_forward_ios,size: 16.0,),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (content)=>GoogleMapWidget()));
                      }
                  )
              ),
            ),

            SizedBox(height: 20.0),

            Container(
              decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(8.0),
                  border: Border.all(color: Colors.grey,width: 1.0)
              ),
              height: 65.0,
              child: ListTile(
                  leading: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: new BorderRadius.circular(5.0),
                        border: Border.all(color: Colors.grey,width: 1.0)
                    ),
                  ),

                  title: RichText(
                    text: TextSpan(
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Guichet Ecobank ",
                          style: TextStyle(fontWeight: FontWeight.w700),
                        ),
                        TextSpan(text: "0.5 km")
                      ],
                    ),
                  ),
                  subtitle: Text("Boulevard de marseille"),
                  trailing: IconButton(
                    icon: Icon(Icons.arrow_forward_ios,size: 16.0,),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (content)=>GoogleMapWidget()));
                      }
                  )
              ),
            ),

            SizedBox(height: 20.0),

            Container(
              decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(8.0),
                  border: Border.all(color: Colors.grey,width: 1.0)
              ),
              height: 65.0,
              child: ListTile(
                  leading: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: new BorderRadius.circular(5.0),
                        border: Border.all(color: Colors.grey,width: 1.0)
                    ),
                  ),

                  title: RichText(
                    text: TextSpan(
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Guichet Ecobank ",
                          style: TextStyle(fontWeight: FontWeight.w700),
                        ),
                        TextSpan(text: "0.5 km")
                      ],
                    ),
                  ),
                  subtitle: Text("Boulevard de marseille"),
                  trailing: IconButton(
                    icon: Icon(Icons.arrow_forward_ios,size: 16.0,),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (content)=>GoogleMapWidget()));
                      }
                  )
              ),
            ),

            SizedBox(height: 20.0),

            Container(
              decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(8.0),
                  border: Border.all(color: Colors.grey,width: 1.0)
              ),
              height: 65.0,
              child: ListTile(
                  leading: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: new BorderRadius.circular(5.0),
                        border: Border.all(color: Colors.grey,width: 1.0)
                    ),
                  ),

                  title: RichText(
                    text: TextSpan(
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Guichet Ecobank ",
                          style: TextStyle(fontWeight: FontWeight.w700),
                        ),
                        TextSpan(text: "0.5 km")
                      ],
                    ),
                  ),
                  subtitle: Text("Boulevard de marseille"),
                  trailing: IconButton(
                    icon: Icon(Icons.arrow_forward_ios,size: 16.0,),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (content)=>GoogleMapWidget()));
                      }
                  )
              ),
            ),

            SizedBox(height: 20.0),

            Container(
              decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(8.0),
                  border: Border.all(color: Colors.grey,width: 1.0)
              ),
              height: 65.0,
              child: ListTile(
                  leading: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: new BorderRadius.circular(5.0),
                        border: Border.all(color: Colors.grey,width: 1.0)
                    ),
                  ),

                  title: RichText(
                    text: TextSpan(
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Guichet Ecobank ",
                          style: TextStyle(fontWeight: FontWeight.w700),
                        ),
                        TextSpan(text: "0.5 km")
                      ],
                    ),
                  ),
                  subtitle: Text("Boulevard de marseille"),
                  trailing: IconButton(
                    icon: Icon(Icons.arrow_forward_ios,size: 16.0,),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (content)=>GoogleMapWidget()));
                      }
                  )
              ),
            ),

            SizedBox(height: 20.0),

            Container(
              decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(8.0),
                  border: Border.all(color: Colors.grey,width: 1.0)
              ),
              height: 65.0,
              child: ListTile(
                  leading: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: new BorderRadius.circular(5.0),
                        border: Border.all(color: Colors.grey,width: 1.0)
                    ),
                  ),

                  title: RichText(
                    text: TextSpan(
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: "Guichet Ecobank ",
                          style: TextStyle(fontWeight: FontWeight.w700),
                        ),
                        TextSpan(text: "0.5 km")
                      ],
                    ),
                  ),
                  subtitle: Text("Boulevard de marseille"),
                  trailing: IconButton(
                    icon: Icon(Icons.arrow_forward_ios,size: 16.0,),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (content)=>GoogleMapWidget()));
                      }
                  )
              ),
            ),
          ],
        ),
      ),
    );
  }
}
